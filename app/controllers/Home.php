<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Sahabat Petani";

    $this->view("template/header",$data);
    $this->view("home/pageHeader");
    $this->view("home/index",$data);
    $this->view("template/footer");
  }
}
