<div class="container">
    <div class="row">
        <div class="col-lg-8 mt-5" id="artikel">
            <h4>Panca Bhakti Sahabat Petani</h4>
            <article>
                <p>
                amet consectetur adipiscing elit ut aliquam purus sit amet luctus venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non enim praesent elementum facilisis leo vel fringilla est ullamcorper eget nulla facilisi etiam dignissim diam quis enim lobortis scelerisque fermentum dui faucibus in ornare quam viverra orci sagittis eu volutpat odio facilisis mauris sit amet massa vitae tortor condimentum lacinia quis vel eros donec ac odio tempor orci dapibus ultrices in iaculis nunc sed augue lacus viverra vitae congue eu consequat ac felis donec et odio pellentesque diam volutpat commodo sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate sapien nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae nunc sed velit dignissim sodales ut eu sem integer vitae justo eget magna fermentum iaculis eu non diam phasellus vestibulum lorem sed risus ultricies tristique nulla aliquet enim tortor at auctor urna nunc id cursus metus aliquam eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci a scelerisque purus semper eget duis at tellus at urna condimentum mattis pellentesque id nibh tortor id aliquet lectus proin nibh nisl condimentum id venenatis a condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
                </p>
            </article>
        </div>
        <div class="col-lg-4 mt-5" id="booking">
            <h4>Pemesanan Alat Pertanian</h4>
            <p>Formulir booking pemesanan peralatan pertanian.</p>
            <p>Informasi lengkap hubungi : 0812 3456 7890</p>
            
            <div class="form">
                <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" name="nama" id="nama" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="nomorhp">Nomor HP</label>
                    <input type="text" name="nomorhp" id="nomorhp" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="pesan">Uraian Pesanan</label>
                    <textarea name="pesan" id="pesan" class="form-control" rows="5" required></textarea>
                </div>

                <div class="form-group">
                    
                    <button class="btn btn-primary">Kirim!</button>
                </div>
            </div>

        </div>
    </div>
</div>